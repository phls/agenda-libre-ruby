# Formatter for diff output
module Differ
  module Format
    # Try to format as unified diff, using - and +
    module Patch
      class << self
        def format(change)
          if change.change?
            as_change change
          elsif change.delete?
            as_delete change
          elsif change.insert?
            as_insert change
          else
            ''
          end
        end

        private

        def as_insert(change)
          "+ #{change.insert}"
        end

        def as_delete(change)
          "- #{change.delete}"
        end

        def as_change(change)
          [as_delete(change), as_insert(change)].join "\n"
        end
      end
    end
  end
end
