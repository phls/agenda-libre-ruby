# Preview all emails at http://localhost:3000/rails/mailers/note_mailer
class NoteMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/note_mailer/notify
  def notify
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    puts 'XXXXXXXX'
    puts Note.all
    puts User.all
    puts Event.count
    puts Note.last.event
    NoteMailer.notify Note.last
  end

  # Preview this email at http://localhost:3000/rails/mailers/note_mailer/create
  def create
    ActionMailer::Base.default_url_options[:host] = 'localhost:3000'
    NoteMailer.create Note.last
  end
end
