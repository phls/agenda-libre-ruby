fr:
  mail_prefix: "[AdL] "
  show: Voir
  save: Enregistrer
  edit: Éditer
  validate: Valider
  refuse: Refuser
  destroy: Supprimer
  logout: Se déconnecter
  staleObjectError: Désolé, votre modification est rejetée car une autre
    personne est déjà intervenue

  # Traductions des écrans
  layouts:
    application:
      login: Authentification
      title: L'Agenda du Libre
      subtitle: Les événements du Libre
      flag: Drapeau
      france: France
      quebec: Québec
      belgique: Belgique
      suisse: Suisse
      propose: Proposer un événement
      propose_orga: Proposer une organisation
      map: Carte
      tags: Mots-clés
      infos: Infos
      stats: Statistiques
      contact: Contact
      moderation: Modération
    mailer:
      title: Bonjour,
  events:
    index:
      calendar_in: Ce calendrier en <em class='fa fa-rss'></em> %{rss}, %{webcal} ou %{ical}
      nb_events: "%{count} événements"
      filter: Filtrage avancé
    show:
      orga-list: Orgas de la région
      add_to_calendar: Ajouter à mon calendrier
      copy: Dupliquer événement
      at: À
      dateAndPlace: Date et lieu
      noMap: Aucune carte n'a pu être associée à cette adresse. Vous pouvez
        tester d'autres syntaxes ici
      description: Description
      infos: Informations
      actions: Actions
      edit: Éditer événement
      cancel: Annuler événement
      html:
        at: À
        dateAndPlace: Date et lieu
        description: Description
        infos: Informations
    new:
      title: Proposer un événement
      subtitle: Cette page permet de soumettre un événement. Celui-ci
        n'apparaîtra pas automatiquement, il sera tout d'abord validé par un
        modérateur. Un courrier électronique sera envoyé à l'adresse e-mail du
        soumetteur donnée ci-dessous lorsque l'événement aura été modéré.
      preview: Prévisualisation
      edit: Création
    create:
      ok: Votre événement a bien été ajouté à la liste des événements en
        attente de modération. Il apparaîtra en ligne dès qu'un modérateur
        l'aura validé.
    edit:
      title: Éditer un événement
      warning: 'Événement déjà modéré: toute modification sera immédiatement
        visible sur le site'
      forbidden: Vous n'êtes pas authorisé à modifier cet événement
      preview: Prévisualisation
      edit: Édition
    preview:
      warning: 'Événement déjà modéré: toute modification sera immédiatement
        visible sur le site'
    update:
      ok: Votre événement a été mis à jour
    form:
      title_helper: Moins de 5 mots, sans lieu ou date
      rule_helper: Les événements répétés seront générés lors de la
        validation. Vous recevrez par mail les liens d'édition et d'annulation
      description_helper: Décrivez de la manière la plus complète possible
        votre événement
      address_helper: "*Associée à la ville et la région, elle générera une
        [carte](http://www.openstreetmap.org), affichée aux côtés de
        l'événement*"
      url_helper: Lien **direct** vers une page donnant plus d'informations sur
        l'événement
      contact_helper: Adresse e-mail de contact, affichée de manière peu
        compréhensible par les spammeurs
      submitter_helper: Adresse e-mail du soumetteur de l'événement. Elle ne
        sera utilisée que par les modérateurs pour contacter la personne ayant
        proposé l'événement, pour l'informer de sa validation ou de son rejet.
        Si cette adresse n'est pas présente, l'adresse de contact sera utilisée
      tags_helper: Séparés par des espaces, constitués de lettres, chiffres et
        tirets. Ajoutez le nom de la ou des organisations de l'événement, mais
        pas de la ville ou de la région.
      save: Valider
      visualise: Visualiser
    cancel:
      title: Annulation de l'événement
      already_moderated: 'Événement déjà modéré: cette annulation le supprimera'
      confirm: Confirmez-vous l'annulation de cet événement?
      preview: Visualisation de l'événement
      ok: Oui
      ko: Non
    destroy:
      ok: Votre événément a bien été annulé
  regions:
    selector:
      all_regions: Toutes les régions
      national: Événements nationaux
    index:
      title: Liste des flux
      quick: Chaque flux liste les événements pour les 30 prochains jours dans
        une région donnée
      help: "Quelques fonctionnalités intéressantes à l'aide de paramètres:\n
        \n
        * `tag`: limiter les événements à un certain tag. Cela permet par
        exemple de récupérer un flux des événements d'une organisation, à
        partir du moment où vous pensez à marquer tous vos événements avec un
        tag précis. \n
        Exemple: `%{tag}`\n
        * `daylimit`: limiter les énévenements futurs à un certain nombre de
        jours. \n
        Exemple: `%{daylimit}` \n
        * `near[location]` et `near[distance]`: limiter les événements autour
        d'un lieu, à une certaine distance (par défaut à 20 km). \n
        Exemple: `%{near}` \n
        \n
        On peut utiliser ces paramètres avec les flux, mais aussi la carte ou
        le calendrier principal"
  stats:
    index:
      title: Statistiques
      allEvents: Événements validés
      allOrgas: Organisations validées
      awaitingModeration: En cours de modération
      dates: Par date
      regional: Par région
      city: Par ville
      city_conditions: Seules les villes où plus de trois événements ont été
        organisés sont mentionnées.
      total: Total
      web: Statistiques Web
  tags:
    index:
      title: Mots-clés
      limited: Seuls les mots-clés portants sur plus de trois événements sont
        affichés dans cette liste
    show:
      links: Voir aussi
      future: Prochainement
      past: Dans le passé
  moderations:
    index:
      title: Événements à modérer
      rules: Modérateurs, merci de lire et de tenir compte des [recommandations
        de modération](/application/rules).
      actions: Actions
      posted_by: Posté par %{author} le %{date}
      date: Date
      askInfos: Demander des infos
      createNote: Ajouter une note
    edit:
      title: Éditer un événement
      moderation: Modération
      warning: 'Événement déjà modéré: toute modification sera
        immédiatement visible sur le site'
      preview: Prévisualisation
      edit: Édition
    update:
      ok: Événement mis à jour
    validate:
      title: Confirmez-vous la validation de cet événement?
      warning: Attention, cet événement est à portée nationale!
      ok: Oui
      ko: Modération
      tweet_helper: Un tweet sera publié, dont voici le contenu
      repeat_helper:
        zero:
        one: Un autre événement sera généré
        other: "%{count} autres événements seront générés"
    accept:
      ok: Événement accepté
    refuse:
      title: Quel motif souhaitez-vous associer au rejet de cet événement?
      motif: Motif
      ok: Rejeter
      ko: Modération
      reason_r_1: Hors sujet
      reason_r_2: Pas assez d'informations
      reason_r_3: Événement déjà enregistré
      reason_r_4: Raison spécifique (précisez)
      reason_r_1_long: Toutefois, l'événement proposé n'a pour l'instant pas
        retenu l'attention des modérateurs. En effet, l'événement proposé ne
        concerne pas le Logiciel Libre, ou bien le lien avec le Logiciel Libre
        n'est pas évident dans la formulation actuelle, ou alors il s'agit d'un
        événement ou d'une formation payante et coûteuse. Si l'événement
        concerne vraiment le Logiciel Libre et qu'il ne s'agit pas d'une
        formation payante, n'hésitez pas à le soumettre à nouveau avec une
        description plus claire.
      reason_r_2_long: "Votre événement a tout à fait sa place ici, mais les
modérateurs trouvent que la description de celui-ci n'est pas assez complète
pour être validée.
\n
\nLa description doit être compréhensible par un nouveau venu dans le monde du
Libre, et doit donc préciser le principe de la rencontre, le public visé, le
rôle du ou des Logiciels Libres qui seront exposés, la date et le lieu précis
de la rencontre. Même si il s'agit d'une rencontre régulière, n'hésitez pas à
répéter à chaque fois ces informations, elles sont importantes.
\n
\nNous vous invitons donc vivement à soumettre à nouveau cet événement avec une
description plus complète."
      reason_r_3_long: Votre événement a tout à fait sa place ici, mais il a
        déjà été enregistré.
      reason_text: Votre raison
    destroy:
      ok: Événement rejeté
  notes:
    new:
      back: Modération
      title: Ajout d'une note de modération
      event: Événements à modérer
    create:
      sendByMailWrap: "<p>Demande d'informations complémentaires:</p>
        <pre>%{contents}</pre>"
      ok: La note a bien été ajoutée, merci!
    form:
      save: Envoyer
      ok: Enregistrer
      ko: Modération
  maps:
    index:
      title: Carte des événements et organisations
      events: Événements
      orgas: Orga
  users:
    sign_in:
      title: Identification
  orgas:
    search:
      title: Trouve ton %{entity}!
      label: Recherche
    index:
      title: Organisations
      new: Ajouter une organisation
    show:
      links: Voir aussi
      actions: Actions
      edit: Éditer
      cancel: Supprimer
      future: Prochainement
      past: Dans le passé
      count:
        zero:
        one: Un événement
        other: "%{count} événements"
    new:
      title: Organisation
      edit: Création
    create:
      ok: L'organisation a été soumise à modération
    edit:
      title: Organisation
      edit: Édition
      forbidden: Vous n'êtes pas authorisé à modifier cette organisation
    update:
      ok: L'organisation a été mise à jour
    form:
      address_helper: "*Associée à la ville et la région, elle générera une
        [carte](http://www.openstreetmap.org), affichée aux côtés de
        l'organisation*"
      url_helper: Lien vers le site web de l'organisation
      feed_helper: Lien **direct** vers un flux de syndication, type RSS ou atom
      contact_helper: Adresse e-mail de contact, affichée de manière peu
        compréhensible par les spammeurs
      submitter_helper: Adresse e-mail du soumetteur de l'organisation.
        Utilisée par les modérateurs pour informer de sa validation ou de son
        rejet. Si cette adresse est présente, l'organisation ne sera modifiable
        que par le soumetteur, qui recevra un lien secret d'édition...
      tags_helper: Séparés par des espaces, constitués de lettres, chiffres et
        tirets
      save: Envoyer
    validate:
      title: Modération d'organisation
      ok: Oui
      ko: Modération
    accept:
      ok: Organisation acceptée
    cancel:
      title: Suppression de l'organisation
      already_moderated: 'Organisation déjà modérée: cela la supprimera'
      confirm: Confirmez-vous la suppression de cette organisation?
      preview: Organisation
      ok: Oui
      ko: Non
    destroy:
      ok: L'organisation a bien été supprimée
  digests:
    markdown:
      title: Agenda du Libre pour la semaine %{week} de l'année %{year}
      week: Calendrier web, regroupant des événements liés au Libre (logiciel, salon, atelier, install party, conférence) en France, annoncés par leurs organisateurs. Voici un récapitulatif de la semaine à venir. Le détail de chacun de ces %{count} événements est en seconde partie de dépêche.

  devise:
    sessions:
      new:
        title: Identification
        sign_in: Identifier

  event_mailer:
    create:
      subject: "Votre événement: '%{subject}' est en attente
        de modération"
      body: "Votre événement intitulé '%{subject}',
\nqui aura lieu le '%{start_time}' a bien été enregistré.
\n
\nL'équipe de modération le prendra en charge très prochainement.
\n
\nPendant la modération et après celle-ci si votre événement est validé, vous
pouvez éditer votre événement à l'adresse:"
      delete_link: "et vous pouvez l'annuler en utilisant l'adresse:"
      signature: Merci de votre participation!
    accept:
      subject: "Événement '%{subject}' modéré"
      body: "L'événement que vous avez soumis a été modéré par %{moderator}. Il
est maintenant visible à l'adresse:"
      edit_link: "Vous pouvez modifier cet événement ultérieurement pour y
ajouter des précisions en vous rendant à l'adresse:"
      delete_link: "Vous pouvez également l'annuler en vous rendant à
l'adresse:"
      repeat_helper:
        zero:
        one: Un autre événement a été généré, voici les liens d'édition et annulation
        other: "%{count} autres événements ont été générés, voici les liens d'édition et annulation"
      signature: Merci de votre contribution et à bientôt!
    destroy:
      subject: "Événement '%{subject}' refusé"
      body: Vous avez soumis l'événement suivant, et nous vous remercions de
        cette contribution.
      reclamation: Pour toute réclamation, n'hésitez pas à contacter l'équipe
        de modérateurs.
      reminder: "Pour rappel, voici le contenu de votre événement:"
      signature: Avec tous nos remerciements pour votre contribution
  moderation_mailer:
    create:
      subject: "Nouvel événement à modérer: '%{subject}'"
      body: Un nouvel événement est à modérer sur
      signature: Merci!
    update:
      subject: "Édition de l'événement '%{subject}'"
      body: "L'événement '%{subject}' a été modifié par
        %{author}.
\n
\nModifications apportées:"
      submitter: le soumetteur
      signature: Bonne journée
    accept:
      subject: "Événement '%{subject}' modéré"
      body: L'événement a été modéré par %{author}.
      access: "Vous pouvez le consulter ici:"
      signature: Merci pour votre contribution!
    destroy:
      subject: "Événement '%{subject}' refusé"
      body: "L'événement '%{subject}' a été rejeté par %{author} pour la raison
        suivante:"
      reminder: "Pour rappel, l'événement:"
      signature: Merci pour votre contribution
  note_mailer:
    notify:
      subject: "Demande d'informations sur l'événement '%{subject}'"
      body: "Nous avons bien reçu votre proposition d'événement '%{subject}',
\net celui-ci a toute sa place ici. Néanmoins, avant d'être  validé, nous
\navons besoin de quelques informations complémentaires sur cet événement:"
      edit_link: "Nous vous invitons à ajouter ces informations en éditant directement
\nl'événement à l'adresse suivante:"
      signature: Avec tous nos remerciements pour votre contribution
    create:
      subject: "Une note a été rajoutée à l'événement '%{subject}'"
      body: "Une note a été rajoutée à '%{subject}':"
      signature: Merci pour votre contribution
  orga_mailer:
    create:
      subject: Organisation '%{subject}' en attente de modération
      body: "Organisation '%{subject}' a bien été enregistrée.
\n
\nL'équipe de modération la prendra en charge très prochainement.
\n
\nPendant la modération et après celle-ci si cette organisation est validée,
\nvous pouvez l'éditer à l'adresse:"
      delete_link: "et vous pouvez l'annuler en utilisant l'adresse:"
      signature: Merci de votre participation!
    update:
      subject: "Organisation '%{subject}' modifiée"
      body: "L'organisation '%{subject}' a été modifiée par %{author}.
\n
\nModifications apportées:"
      submitter: le soumetteur
      access: "Vous pouvez la consulter ici:"
      signature: Bonne journée
    accept:
      subject: "Organisation '%{subject}' modérée"
      body: L'organisation a été modérée par %{author}.
      access: "Vous pouvez la consulter ici:"
      signature: Merci pour votre contribution!
    destroy:
      subject: Organisation '%{subject}' supprimée
      body: Vous avez soumis l'organisation suivante, et nous vous remercions
        de cette contribution.
      reclamation: Pour toute réclamation, n'hésitez pas à contacter l'équipe
        de modérateurs.
      reminder: "Pour rappel, voici le contenu de l'organisation:"
      signature: Avec tous nos remerciements pour votre contribution

  versions:
    index:
      title: Versions
      create_html: <em class='fa fa-plus-circle'></em>
      update_html: <em class='fa fa-exchange'></em>
      destroy_html: <em class='fa fa-trash'></em>
      feed: Flux RSS

  application:
    filter:
      title: Filtrage avancé
      helper: Plusieurs paramètres sont prévus dans l'agenda, en consultation ou pour l'intégrer à un autre site
      past: Passés
      past_helper: Inclure aussi les événements passés dans les flux RSS, carte et geojson
      period_year: Année
      period_year_helper: Pour définir la semaine/année du résumé
      period_week: Semaine
      near_location: Près de
      near_location_helper: ville ou lieu géographique
      near_distance: Distance
      near_distance_helper: du lieu
      region: Région
      tag: Mot-clé
      iframe: Sans cadre
      iframe_helper: Supprime l'en-tête et bas de page, pour intégration dans une iframe
      events: Agenda
      map: Carte
      geojson: GeoJSON
      json: JSON
      rss: RSS
      ics: iCal
      xml: XML
      digest: Résumé
      orgas: Orgas
