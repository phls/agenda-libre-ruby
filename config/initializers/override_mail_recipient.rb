# config/initializers/override_mail_recipient.rb
if Rails.env.development?
  # During development, we don't really send mails to the outside world
  class OverrideMailRecipient
    def self.delivering_email(mail)
      mail.to = 'manu@localhost'
    end
  end
  ActionMailer::Base.register_interceptor(OverrideMailRecipient)
end
