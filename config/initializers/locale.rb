require 'i18n/backend/active_record'

# https://github.com/dylanz/i18n_backend_database
Translation = I18n::Backend::ActiveRecord::Translation

if Translation.table_exists?
  I18n.backend = I18n::Backend::Chain.new(I18n::Backend::ActiveRecord.new,
                                          I18n.backend)
  I18n::Backend::ActiveRecord.send :include, I18n::Backend::Memoize
end
