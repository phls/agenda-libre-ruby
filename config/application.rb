require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AgendaDuLibreRails
  # All the specific configuraton for ADL
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified
    # here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record
    # auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names.
    # Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from
    # config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path +=
    #   Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.load_path +=
      Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    # config.i18n.default_locale = :de
    config.i18n.default_locale = :fr
    config.i18n.available_locales = [:fr, :en, :'pt-BR']

    config.action_mailer.default_options = {
      from: 'moderateurs@agendadulibre.org',
      to: 'moderateurs@agendadulibre.org'
    }

    # config.quiet_assets = false
    config.sass.preferred_syntax = :sass

    config.action_dispatch.default_headers['X-Frame-Options'] = 'ALLOWALL'

    config.active_record.raise_in_transactional_callbacks = true

    # In rails 4, plugin and vendor images need to be precompiled
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
  end
end
