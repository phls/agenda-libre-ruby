(function() {
  $(document).on('turbolinks:load', function() {
    return tinyMCE.init({
      schema: 'html5',
      menubar: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-2206cdbaf19b5e0a020de5739e2c911351d6bdd80a0dac961f79729f718ae851.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      toolbar: [' bold italic strikethrough | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | link image media insertdatetime charmap table | undo redo | searchreplace | code visualblocks preview fullscreen'],
      plugins: 'lists, advlist, autolink, link, image, charmap, paste, print, preview, table, fullscreen, searchreplace, media, insertdatetime, visualblocks, visualchars, wordcount, contextmenu, code'
    });
  });

  $(document).on('turbolinks:before-cache', function() {
    return tinymce.remove();
  });

}).call(this);
