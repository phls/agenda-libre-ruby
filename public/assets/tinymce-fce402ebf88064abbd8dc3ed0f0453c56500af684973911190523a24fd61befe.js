(function() {
  $(document).ready(function() {
    return tinyMCE.init({
      schema: 'html5',
      menubar: false,
      language: 'fr_FR',
      selector: 'textarea.description',
      content_css: '/assets/application-d75ab3978be89f2612f0a6d34ebc36e5ff5906daaebebbf9d3e573ca27ad3421.css',
      entity_encoding: 'raw',
      add_unload_trigger: true,
      browser_spellcheck: true,
      toolbar: [' bold italic strikethrough | bullist numlist outdent indent | alignleft aligncenter alignright alignjustify | link image media insertdatetime charmap table | undo redo | searchreplace | code visualblocks preview fullscreen'],
      plugins: 'lists, advlist, autolink, link, image, charmap, paste, print, preview, table, fullscreen, searchreplace, media, insertdatetime, visualblocks, visualchars, wordcount, contextmenu, code'
    });
  });

  $(document).on('page:receive', function() {
    return tinymce.remove();
  });

}).call(this);
