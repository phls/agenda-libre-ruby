# Cleans up filter submission, for cleaner URL
$(document).on 'turbolinks:load', ->
	$('body.filter form :input').prop 'disabled', false

	$('body.filter form').submit ->
		$('input[name=utf8]').prop 'disabled', true
		$(':input', this).filter ->
			this.value.length == 0
		.prop 'disabled', true
