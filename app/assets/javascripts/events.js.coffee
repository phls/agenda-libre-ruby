$(document).on 'turbolinks:load', ->
  # Manage the tags label so it points the proper select2 input
  $('label[for=event_tags]').attr 'for', 's2id_autogen1'

  $('#event_start_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_end_time').val($('#event_start_time').val())
  $('#event_end_time').change ->
    if $('#event_start_time').val() >= $('#event_end_time').val()
      $('#event_start_time').val($('#event_end_time').val())

	# Quick mechanism so that the ice cube rule only appears when useful
  $('#event_repeat').each ->
    if $(this).val() == '0'
      $('.field.rule').hide()

    $(this).change ->
      if $(this).val() > 0
        $('.field.rule').show()
      else
        $('.field.rule').hide()

  # Manage event tags edition
  $('#event_tags').each ->
    elt = $(this)
    $.ajax
      url: '/tags.json'
    .done (data) ->
      tags = jQuery.map data, (n) -> n[0]

      elt.select2 tags: tags, separator: [' '], tokenSeparators: [' ']
