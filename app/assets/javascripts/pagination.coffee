# Automatic ajax pagination

# To not setup pagination twice
pager = true
$(document).on 'turbolinks:load', ->
	$('.pagination .next a')
		.attr('data-remote', true)
		.click ->
			$('#loading').fadeIn()

	if pager
		pager = false
		$(document).on 'ajax:success', '.pagination .next a', (event, data, status, xhr) ->
			$('#loading').fadeOut()
			elts = $('tbody tr', data)
			$(this).parents('tfoot').prev().append(elts)

			next = $('.pagination .next a', data).attr('href')
			if next?
				return $(this).show().data('remote', true).attr('href', next)
			else
				return $(this).parents('.pagination').remove()

	# Go to the next page when page is scrolled
	if $('.pagination .next a').size() > 0
		$(document).scroll ->
			if $(window).scrollTop() == $(document).height() - $(window).height() && $('.pagination .next a').is(':visible')
				# "Next" link is also hidden while pagination is done
				$('.pagination .next a').hide().click()
