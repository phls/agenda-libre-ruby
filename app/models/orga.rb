# Organisations related to this agenda
class Orga < ActiveRecord::Base
  strip_attributes
  has_paper_trail ignore: [:last_updated, :secret, :submitter, :decision_time,
                           :lock_version, :latitude, :longitude]

  belongs_to :region
  belongs_to :kind

  validates :kind, presence: true
  validates :name, presence: true
  validates :region, presence: true
  validates :url, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :diaspora, allow_blank: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :feed, allow_blank: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :contact, allow_blank: true, email: true
  validates :submitter, allow_blank: true, email: true

  geocoded_by :full_address
  # after_validation :geocode, if: -> (obj) { obj.address_changed? }
  after_validation :geocode

  scope :active, -> { where active: true }
  scope :moderated, -> { where moderated: true }
  scope :unmoderated, -> { where moderated: false }

  # Only present to simplify maps_controller, to have the same scopes as events
  scope :locality, -> {}
  scope :daylimit, -> {}
  scope :future, -> {}
  scope :period, ->(_year, _week) {}

  scope :region, ->(region) { where region: region unless region == 'all' }
  scope :tag, ->(tag) { where 'tags like ?', "%#{tag}%" }
  scope :geo, -> { where 'latitude is not null and longitude is not null' }

  before_validation do
    unless submitter.blank?
      self.secret ||= SecureRandom.urlsafe_base64(32)[0...32]
    end
    self.submission_time ||= Time.zone.now
  end

  after_create do
    send_secret unless submitter.blank?
  end

  after_update do
    unless submitter.blank?
      send_secret if secret_changed?

      if moderated_changed?
        OrgaMailer.accept(self).deliver_now!
# Send email to moderators when an orga is accepted
        ModerationorgaMailer.accept(self).deliver_now!
      else
        OrgaMailer.update(self).deliver_now!
# Send email to moderators when an orga is updated
        ModerationorgaMailer.update(self).deliver_now!
      end
    end
  end

  before_destroy do
    OrgaMailer.destroy(self).deliver_now! unless submitter.blank?
# Send email to moderators when an orga is deleted
    ModerationorgaMailer.destroy(self).deliver_now! unless submitter.blank?
  end

  def send_secret
    OrgaMailer.create(self).deliver_now!
# Send email to moderators when an new orga is received
    ModerationorgaMailer.create(self).deliver_now!
  end

  def name_as_tag
    name.gsub(/\AL'/, '').gsub(/[\s\*']/, '-').delete ':'
  end

  def as_json(_options = {})
    { type: 'Feature', properties: {
      id: id, icon: kind.icon, name: name,
      place_name: place_name, address: address, city: city,
      tags: tags,
      popupContent: "<a href=\"/orgas/#{id}\">#{self}</a>"
    }, geometry: { type: 'Point', coordinates: [longitude, latitude] } }
  end

  def full_address
    # Region seems to disturb openstreetmap :(
    # [address, city, region.try(:name)].compact.join ', '
    [address, city].compact.join ', '
  end

  def to_s
    "[#{kind.name}] #{name}"
  end
end
