xml.instruct!

xml.rdf :RDF,
        'xmlns:rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
        'xmlns' => 'http://purl.org/rss/1.0/',
        'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
        'xmlns:sy' => 'http://purl.org/rss/1.0/modules/syndication/',
        'xmlns:admin' => 'http://webns.net/mvcb/',
        'xmlns:cc' => 'http://web.resource.org/cc/',
        'xmlns:content' => 'http://purl.org/rss/1.0/modules/content/',
        'xmlns:georss' => 'http://www.georss.org/georss' do
  xml.channel 'rdf:about' => root_url do
    title = t 'layouts.application.title'
    if params[:region].present? && params[:region] != 'all'
      region = Region.find(params[:region]).name
      title += " [#{region}]"
    end
    xml.title title
    xml.description t 'layouts.application.subtitle'
    xml.link root_url
    xml.dc :language, 'fr'
    xml.dc :creator, root_url

    xml.items do
      xml.rdf :Seq do
        @events.each do |event|
          xml.rdf :li, 'rdf:resource' => event_url(event)
        end
      end
    end
  end

  @events.each do |event|
    xml.item 'rdf:about' => event_url(event) do
      xml.title "#{event.city}: #{event.title}, #{display_date event}"
      xml.link event_url event
      domain = root_url.gsub(/www/, '').gsub(/http.?:../, '').gsub(/:.*/, '')
      xml.dc :identifier, "#{event.id}@#{domain}"
      xml.dc :date, event.start_time.iso8601
      if event.latitude && event.longitude
        xml.georss :point, event.latitude, ' ', event.longitude
      end

      xml.description strip_tags event.description
      xml.content(:encoded) { xml.cdata! event.description }
    end
  end
end
