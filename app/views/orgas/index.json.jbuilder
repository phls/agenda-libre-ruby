json.array!(@orgas) do |orga|
  json.extract! orga, :id, :name, :description,
                :place_name, :address, :city, :region_id, :url,
                :contact, :tags
  json.url orga_url(orga, format: :json)
end
