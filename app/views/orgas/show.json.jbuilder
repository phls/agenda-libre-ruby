json.extract! @orga, :id, :name, :description,
              :place_name, :address, :city, :region, :url, :contact, :tags
