# Sending mails related to events' moderation
class ModerationMailer < ApplicationMailer
  helper :events

  def create(event)
    @event = event

    mail 'Message-ID' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderation_mailer.create.subject',
                                         subject: event.title}"
  end

  def update(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderation_mailer.update.subject',
                                         subject: event.title}"
  end

  def accept(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderation_mailer.accept.subject',
                                         subject: event.title}"
  end

  def destroy(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail.originator

    mail 'In-Reply-To' =>
      "<mod-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderation_mailer.destroy.subject',
                                         subject: event.title}"
  end
end
