# Send mails to check on organisations life cycle
class ModerationorgaMailer < ApplicationMailer
  helper :events

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.orga_mailer.create.subject
  #
  def create(orga)
    @orga = orga

    mail 'Message-ID' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderationorga_mailer.create.subject',
                                         subject: orga.name}"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.orga_mailer.update.subject
  #
  def update(orga)
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderationorga_mailer.update.subject',
                                         subject: orga.name}"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.orga_mailer.accept.subject
  #
  def accept(orga)
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderationorga_mailer.accept.subject',
                                         subject: orga.name}"
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.orga_mailer.destroy.subject
  #
  def destroy(orga, reason = '')
    @orga = orga
    @current_user = User.find_by id: orga.paper_trail.originator
    @reason = reason

    mail 'In-Reply-To' =>
      "<orga-#{orga.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'moderationorga_mailer.destroy.subject',
                                         subject: orga.name}"
  end
end
