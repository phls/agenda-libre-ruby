# Sending mails related to events' notes
class NoteMailer < ApplicationMailer
  helper :events

  def notify(note)
    @note = note

    host = ActionMailer::Base.default_url_options[:host]
    mail 'In-Reply-To' =>
      "<event-#{note.event.id}@#{host}>",
         to: note.event.submitter,
         subject: "#{t 'mail_prefix'}#{t 'note_mailer.notify.subject',
                                         subject: note.event.title}"
  end

  def create(note)
    @note = note

    mail 'In-Reply-To' =>
      "<mod-#{note.event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         subject: "#{t 'mail_prefix'}#{t 'note_mailer.create.subject',
                                         subject: note.event.title}"
  end
end
