ActiveAdmin.register City do
  permit_params :name, :majname, :postalcode, :inseecode, :regioncode,
                :latitude, :longitude
end
